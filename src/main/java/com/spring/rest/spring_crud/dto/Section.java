package com.spring.rest.spring_crud.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.LinkedHashSet;

@NoArgsConstructor
@Getter
@Setter
public class Section implements Serializable {

    private LinkedHashSet<String> sections;

}
