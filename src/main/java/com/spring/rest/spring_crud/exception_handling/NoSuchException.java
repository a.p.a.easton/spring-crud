package com.spring.rest.spring_crud.exception_handling;

public class NoSuchException extends RuntimeException{

    public NoSuchException(String message) {
        super(message);
    }

}
