package com.spring.rest.spring_crud.service;

import com.spring.rest.spring_crud.entity.Skill;

import java.util.List;

public interface SkillService {

    List<Skill> getAllSkills();

    Skill saveSkill(Skill skill);

    void saveSkillWithEmployees(Skill skill);

    Skill getSkill(int id);

    void deleteSkill(int id);

}
