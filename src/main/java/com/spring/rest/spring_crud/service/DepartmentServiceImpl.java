package com.spring.rest.spring_crud.service;

import com.spring.rest.spring_crud.entity.Department;
import com.spring.rest.spring_crud.exception_handling.NoSuchException;
import com.spring.rest.spring_crud.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentService{

    private DepartmentRepository departmentRepository;

    @Autowired
    public void setDepartmentRepository(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public List<Department> getAllDepartments() {
        return departmentRepository.findAll();
    }

    @Override
    public void saveDepartment(Department department) {
        departmentRepository.save(department);
    }

    @Override
    public Department getDepartment(int id) {
        Optional<Department> optional = departmentRepository.findById(id);
        if(optional.isEmpty()){
            throw new NoSuchException("There is no department with ID = " + id + " in database");
        }
        return optional.get();
    }

    @Override
    public void deleteDepartment(int id) {
        departmentRepository.deleteById(id);
    }
}
