package com.spring.rest.spring_crud.service;

import com.spring.rest.spring_crud.entity.Department;
import com.spring.rest.spring_crud.entity.Employee;

import java.util.List;

public interface DepartmentService {

    List<Department> getAllDepartments();

    void saveDepartment(Department department);

    Department getDepartment(int id);

    void deleteDepartment(int id);

}
