package com.spring.rest.spring_crud.service;

import com.spring.rest.spring_crud.dto.Section;
import com.spring.rest.spring_crud.entity.Catalog;

import java.util.List;
import java.util.Set;

public interface CatalogService {

    List<Catalog> getCatalog();

    Catalog saveCatalog(Section section);

    Catalog getCatalog(int id);

    void deleteCatalog(int id);

}
