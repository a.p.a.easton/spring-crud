package com.spring.rest.spring_crud.service;

import com.spring.rest.spring_crud.entity.Skill;
import com.spring.rest.spring_crud.exception_handling.NoSuchException;
import com.spring.rest.spring_crud.repository.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SkillServiceImpl implements SkillService{

    private SkillRepository skillRepository;

    @Autowired
    public void setSkillRepository(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public List<Skill> getAllSkills() {
        return skillRepository.findAll();
    }

    @Override
    public Skill saveSkill(Skill skill) {
        return skillRepository.save(skill);
    }

    @Override
    public void saveSkillWithEmployees(Skill skill) {
        skillRepository.save(skill);
    }

    @Override
    public Skill getSkill(int id) {
        Optional<Skill> optional = skillRepository.findById(id);
        if(optional.isEmpty()){
            throw new NoSuchException("There is no skill with ID = " + id + " in database");
        }
        return optional.get();
    }

    @Override
    public void deleteSkill(int id) {
        skillRepository.deleteById(id);
    }
    
}
