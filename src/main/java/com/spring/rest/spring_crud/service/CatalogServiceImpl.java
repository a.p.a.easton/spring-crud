package com.spring.rest.spring_crud.service;

import com.spring.rest.spring_crud.dto.Section;
import com.spring.rest.spring_crud.entity.Catalog;
import com.spring.rest.spring_crud.exception_handling.NoSuchException;
import com.spring.rest.spring_crud.repository.CatalogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CatalogServiceImpl implements CatalogService{

    private CatalogRepository catalogRepository;

    @Autowired
    public void setCatalogRepository(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    @Override
    public List<Catalog> getCatalog() {
        return catalogRepository.findAll();
    }

    @Override
    public Catalog saveCatalog(Section section) {
        Catalog catalog;
        Catalog existingCatalog = null;
        Catalog newCatalog;
        for(String s : section.getSections()){
            catalog = catalogRepository.findCatalogBySection(s);
            if(catalog == null){
                newCatalog = new Catalog();
                newCatalog.setSection(s);
                newCatalog.setParent(existingCatalog);
                if(existingCatalog != null){
                    existingCatalog.addChild(newCatalog);
                }
                existingCatalog = catalogRepository.save(newCatalog);
            }
            else{
                existingCatalog = catalog;
            }
        }
        return catalogRepository.findCatalogBySection(section.getSections().toArray(new String[0])[0]);
    }

    @Override
    public Catalog getCatalog(int id) {
        Optional<Catalog> optional = catalogRepository.findById(id);
        if(optional.isEmpty()){
            throw new NoSuchException("There is no catalog with ID = " + id + " in database");
        }
        return optional.get();
    }

    @Override
    public void deleteCatalog(int id) {
        catalogRepository.deleteById(id);
    }

}
