package com.spring.rest.spring_crud.controller;

import com.spring.rest.spring_crud.dto.Section;
import com.spring.rest.spring_crud.entity.Catalog;
import com.spring.rest.spring_crud.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class CatalogController {

    private CatalogService catalogService;

    @Autowired
    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    /*@GetMapping("/catalog")
    public List<Catalog> getAllCatalogs(){
        return catalogService.getCatalog();
    }*/

    @GetMapping("/catalog/{id}")
    public Catalog getCatalogById(@PathVariable int id){
        return catalogService.getCatalog(id);
    }

    @PostMapping("/catalog")
    public Catalog addNewCatalog(@Valid @RequestBody Section section){
        return catalogService.saveCatalog(section);
    }

    /*@PutMapping("/catalog/{id}/change-parent-catalog/{parentId}")
    public Catalog updateCatalog(@PathVariable int id,
                               @PathVariable int parentId) {
        Catalog catalog = catalogService.getCatalog(id);
        Catalog parentCatalog = catalogService.getCatalog(parentId);
        catalog.setParent(parentCatalog);
        return catalogService.saveCatalog(catalog);
    }*/

    @DeleteMapping("/catalog/{id}")
    public String deleteCatalog(@PathVariable int id){
        catalogService.getCatalog(id);
        catalogService.deleteCatalog(id);
        return "Catalog with id = " + id + " was deleted";
    }

}
