package com.spring.rest.spring_crud.controller;

import com.spring.rest.spring_crud.entity.Skill;
import com.spring.rest.spring_crud.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class SkillController {

    private SkillService skillService;

    @Autowired
    public void setSkillService(SkillService skillService) {
        this.skillService = skillService;
    }

    @GetMapping("/skills")
    public List<Skill> getAllSkills(){
        return skillService.getAllSkills();
    }

    @GetMapping("/skills/{id}")
    public Skill getSkillById(@PathVariable int id){
        return skillService.getSkill(id);
    }

    @PostMapping("/skills")
    public Skill addNewSkill(@RequestBody Skill skill){
        return skillService.saveSkill(skill);
    }

    @PutMapping("/skills")
    public Skill updateSkill(@RequestBody Skill skill){
        skillService.getSkill(skill.getId());
        return skillService.saveSkill(skill);
    }

    @DeleteMapping("/skills/{id}")
    public String deleteSkill(@PathVariable int id){
        skillService.getSkill(id);
        skillService.deleteSkill(id);
        return "Skill with id = " + id + " was deleted";
    }

}
