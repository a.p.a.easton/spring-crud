package com.spring.rest.spring_crud.controller;

import com.spring.rest.spring_crud.entity.Department;
import com.spring.rest.spring_crud.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DepartmentController {

    private DepartmentService departmentService;

    @Autowired
    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/departments")
    public List<Department> employees() {
        return departmentService.getAllDepartments();
    }

    @GetMapping("/departments/{id}")
    public Department getEmployee(@PathVariable int id) {
        return departmentService.getDepartment(id);
    }

    @PostMapping("/departments")
    public Department addNewEmployee(@RequestBody Department department) {
        departmentService.saveDepartment(department);
        return department;
    }

    @PutMapping("/departments")
    public Department updateEmployee(@RequestBody Department department) {
        departmentService.saveDepartment(department);
        return department;
    }

    @DeleteMapping("/departments/{id}")
    public String deleteEmployee(@PathVariable int id) {
        departmentService.getDepartment(id);
        departmentService.deleteDepartment(id);
        return "Department with id = " + id + " was deleted";
    }

}
