package com.spring.rest.spring_crud.controller;


import com.spring.rest.spring_crud.entity.Department;
import com.spring.rest.spring_crud.entity.Employee;
import com.spring.rest.spring_crud.entity.Skill;
import com.spring.rest.spring_crud.service.DepartmentService;
import com.spring.rest.spring_crud.service.EmployeeService;
import com.spring.rest.spring_crud.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    private EmployeeService employeeService;

    private DepartmentService departmentService;

    private SkillService skillService;

    @Autowired
    public EmployeeController(EmployeeService employeeService, DepartmentService departmentService, SkillService skillService) {
        this.employeeService = employeeService;
        this.departmentService = departmentService;
        this.skillService = skillService;
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployee(@PathVariable int id) {
        return employeeService.getEmployee(id);
    }

    @PostMapping("/employees")
    public Employee addNewEmployee(@Valid @RequestBody Employee employee) {
        employeeService.saveEmployee(employee);
        return employee;
    }

    @PutMapping("/employees")
    public Employee updateEmployee(@Valid @RequestBody Employee employee) {
        employeeService.saveEmployee(employee);
        return employee;
    }

    @PutMapping("/employees/{empId}/add-employee-to-department/{departmentId}")
    public Employee addEmployeeToDepartment(@PathVariable int departmentId,
                                   @PathVariable int empId) {
        Department department = departmentService.getDepartment(departmentId);
        Employee employee = employeeService.getEmployee(empId);
        employee.setDepartment(department);
        employeeService.saveEmployee(employee);
        return employee;
    }

    @PutMapping("/employees/{empId}/add-skill-to-employee/{skillId}")
    public Employee addSkillToEmployee(@PathVariable int skillId,
                                   @PathVariable int empId) {
        Skill skill = skillService.getSkill(skillId);
        Employee employee = employeeService.getEmployee(empId);
        employee.addSkillToEmployee(skill);
        employeeService.saveEmployee(employee);
        return employee;
    }

    @DeleteMapping("/employees/{id}")
    public String deleteEmployee(@PathVariable int id) {
        Employee employee = employeeService.getEmployee(id);

        employeeService.deleteEmployee(id);
        return "Employee with id = " + id + " was deleted";
    }

}
