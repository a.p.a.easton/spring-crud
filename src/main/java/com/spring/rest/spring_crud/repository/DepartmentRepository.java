package com.spring.rest.spring_crud.repository;

import com.spring.rest.spring_crud.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {
}
