package com.spring.rest.spring_crud.repository;

import com.spring.rest.spring_crud.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Page<Employee> findByDepartmentId(int departmentId, Pageable pageable);
}
