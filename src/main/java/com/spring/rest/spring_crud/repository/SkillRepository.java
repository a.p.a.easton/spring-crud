package com.spring.rest.spring_crud.repository;

import com.spring.rest.spring_crud.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SkillRepository extends JpaRepository<Skill, Integer> {
}
