package com.spring.rest.spring_crud.repository;

import com.spring.rest.spring_crud.entity.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CatalogRepository extends JpaRepository<Catalog, Integer> {
    Catalog findCatalogBySection(String section);
}
